-- This script needs to run only once

DROP DATABASE IF EXISTS FACADE_DB;
CREATE DATABASE FACADE_DB;

USE FACADE_DB;

DROP USER IF EXISTS TheUser@localhost;

CREATE USER TheUser@'localhost' IDENTIFIED WITH mysql_native_password BY 'pancake' REQUIRE NONE;
GRANT ALL ON FACADE_DB.* TO TheUser@'localhost';

FLUSH PRIVILEGES;

