package com.kenfogel.facade_demo.persistence;

import com.kenfogel.facade_demo.data.TeaBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tea table DAO
 *
 * @author Ken
 */
public class TeaDAOImpl implements TeaDAO {

    private final static Logger LOG = LoggerFactory.getLogger(TeaDAOImpl.class);

    private final String url = "jdbc:mysql://localhost:3306/FACADE_DB?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "TheUser";
    private final String password = "pancake";

    public TeaDAOImpl() {
        super();
    }

    @Override
    public ArrayList<TeaBean> getAllTeas() throws SQLException {

        ArrayList<TeaBean> rows = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, user,
                password);
                PreparedStatement pStatement = connection.prepareStatement("SELECT * FROM TEA");
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                TeaBean tb = new TeaBean();
                tb.setId(resultSet.getLong("ID"));
                tb.setTeaName(resultSet.getString("NAMEOFTEA"));
                tb.setCountryOfOrigin(resultSet.getString("COUNTRYOFORIGIN"));
                rows.add(tb);
            }
            LOG.info("Records added to ArrayList : " + rows.size());
            return rows;
        }
    }
}
