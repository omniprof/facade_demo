package com.kenfogel.facade_demo.persistence;

import com.kenfogel.facade_demo.data.CheeseBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cheese table DAO
 *
 * @author Ken
 */
public class CheeseDAOImpl implements CheeseDAO {

    private final static Logger LOG = LoggerFactory.getLogger(CheeseDAOImpl.class);

    private final String url = "jdbc:mysql://localhost:3306/FACADE_DB?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "TheUser";
    private final String password = "pancake";

    public CheeseDAOImpl() {
        super();
    }

    @Override
    public ArrayList<CheeseBean> getAllCheeses() throws SQLException {

        ArrayList<CheeseBean> rows = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, user,
                password);
                PreparedStatement pStatement = connection.prepareStatement("SELECT * FROM CHEESE");
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                CheeseBean cb = new CheeseBean();
                cb.setId(resultSet.getLong("ID"));
                cb.setCheeseName(resultSet.getString("NAMEOFCHEESE"));
                cb.setCountryOfOrigin(resultSet.getString("COUNTRYOFORIGIN"));
                rows.add(cb);
            }
            LOG.info("Records added to ArrayList : " + rows.size());
            return rows;
        }
    }
}
