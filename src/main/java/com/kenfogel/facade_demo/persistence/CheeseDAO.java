package com.kenfogel.facade_demo.persistence;

import com.kenfogel.facade_demo.data.CheeseBean;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Ken
 */
public interface CheeseDAO {

    ArrayList<CheeseBean> getAllCheeses() throws SQLException;

}
