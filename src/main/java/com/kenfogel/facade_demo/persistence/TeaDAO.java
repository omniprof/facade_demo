package com.kenfogel.facade_demo.persistence;

import com.kenfogel.facade_demo.data.TeaBean;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Ken
 */
public interface TeaDAO {

    ArrayList<TeaBean> getAllTeas() throws SQLException;
}
