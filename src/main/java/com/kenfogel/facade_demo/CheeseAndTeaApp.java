package com.kenfogel.facade_demo;

import com.kenfogel.facade_demo.data.CheeseAndTeaBean;
import com.kenfogel.facade_demo.data.CheeseBean;
import com.kenfogel.facade_demo.data.TeaBean;
import com.kenfogel.facade_demo.facade.CheeseAndTeaFacade;
import java.sql.SQLException;

/**
 * Use the Facade to get me some tea and cheese
 *
 * @author Ken
 */
public class CheeseAndTeaApp {

    public void perfom() throws SQLException {
        CheeseAndTeaFacade cheeseAndTea = new CheeseAndTeaFacade();
        CheeseAndTeaBean cheeseAndTeaList = cheeseAndTea.cheeseAndTeaLister();

        System.out.println("\nThe cheese:\n");
        cheeseAndTeaList.getCheese().forEach((cb) -> {
            System.out.println(cb);
        });
        System.out.println("\nThe tea:\n");
        cheeseAndTeaList.getTea().forEach((tb) -> {
            System.out.println(tb);
        });

    }

    public static void main(String[] args) throws SQLException {
        CheeseAndTeaApp cta = new CheeseAndTeaApp();
        cta.perfom();
        System.exit(0);
    }

}
