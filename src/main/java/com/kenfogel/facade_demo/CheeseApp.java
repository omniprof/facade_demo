package com.kenfogel.facade_demo;

import com.kenfogel.facade_demo.listers.CheeseLister;
import com.kenfogel.facade_demo.data.CheeseBean;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * get me some cheese
 *
 */
public class CheeseApp {
    
    public void perform() throws SQLException {
        CheeseLister cheese = new CheeseLister();
        ArrayList<CheeseBean> cheeseList = cheese.getSomeCheese();
        System.out.println("The cheese:\n");
        cheeseList.forEach((cb) -> {
            System.out.println(cb);
        });
        
    }

    public static void main(String[] args) throws SQLException {
        CheeseApp ca = new CheeseApp();
        ca.perform();
        System.exit(0);
    }
}
