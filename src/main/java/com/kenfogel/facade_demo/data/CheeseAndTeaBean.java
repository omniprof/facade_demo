package com.kenfogel.facade_demo.data;

import java.util.ArrayList;

/**
 * All the cheese and tea
 *
 * @author Ken
 */
public class CheeseAndTeaBean {

    private ArrayList<CheeseBean> cheese;
    private ArrayList<TeaBean> tea;

    public ArrayList<CheeseBean> getCheese() {
        return cheese;
    }

    public void setCheese(ArrayList<CheeseBean> cheese) {
        this.cheese = cheese;
    }

    public ArrayList<TeaBean> getTea() {
        return tea;
    }

    public void setTea(ArrayList<TeaBean> tea) {
        this.tea = tea;
    }
}
