package com.kenfogel.facade_demo.data;

import java.util.Objects;

/**
 * All the cheese
 *
 * @author Ken
 */
public class CheeseBean {

    private long id;
    private String cheeseName;
    private String countryOfOrigin;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCheeseName() {
        return cheeseName;
    }

    public void setCheeseName(String cheeseName) {
        this.cheeseName = cheeseName;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 11 * hash + Objects.hashCode(this.cheeseName);
        hash = 11 * hash + Objects.hashCode(this.countryOfOrigin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CheeseBean other = (CheeseBean) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.cheeseName, other.cheeseName)) {
            return false;
        }
        return Objects.equals(this.countryOfOrigin, other.countryOfOrigin);
    }

    @Override
    public String toString() {
        return "CheeseBean{" + "id=" + id + ", cheeseName=" + cheeseName + ", countryOfOrigin=" + countryOfOrigin + '}';
    }

}
