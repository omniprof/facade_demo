package com.kenfogel.facade_demo.data;

import java.util.Objects;

/**
 * All the tea
 * @author Ken
 */
public class TeaBean {

    private long id;
    private String teaName;
    private String countryOfOrigin;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeaName() {
        return teaName;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 13 * hash + Objects.hashCode(this.teaName);
        hash = 13 * hash + Objects.hashCode(this.countryOfOrigin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TeaBean other = (TeaBean) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.teaName, other.teaName)) {
            return false;
        }
        return Objects.equals(this.countryOfOrigin, other.countryOfOrigin);
    }

    @Override
    public String toString() {
        return "TeaBean{" + "id=" + id + ", teaName=" + teaName + ", countryOfOrigin=" + countryOfOrigin + '}';
    }

}
