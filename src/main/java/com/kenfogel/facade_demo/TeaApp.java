package com.kenfogel.facade_demo;

import com.kenfogel.facade_demo.listers.TeaLister;
import com.kenfogel.facade_demo.data.TeaBean;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * get me some tea, please
 *
 */
public class TeaApp 
{
    
    public void perform() throws SQLException {
        TeaLister tea = new TeaLister();
        ArrayList<TeaBean> teaList = tea.getSomeTea();
        System.out.println("The tea:\n");
        teaList.forEach((tb) -> {
            System.out.println(tb);
        });
        
    }
    public static void main(String[] args) throws SQLException {
        TeaApp ta = new TeaApp();
        ta.perform();
        System.exit(0);
    }
}
