package com.kenfogel.facade_demo.listers;

import com.kenfogel.facade_demo.data.TeaBean;
import com.kenfogel.facade_demo.persistence.TeaDAO;
import com.kenfogel.facade_demo.persistence.TeaDAOImpl;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Get all the tea
 *
 * @author Ken
 */
public class TeaLister {

    private final TeaDAO teaGetter;

    public TeaLister() {
        teaGetter = new TeaDAOImpl();
    }

    public ArrayList<TeaBean> getSomeTea() throws SQLException {
        return teaGetter.getAllTeas();
    }
}
