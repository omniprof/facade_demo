package com.kenfogel.facade_demo.listers;

import com.kenfogel.facade_demo.data.CheeseBean;
import com.kenfogel.facade_demo.persistence.CheeseDAO;
import com.kenfogel.facade_demo.persistence.CheeseDAOImpl;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Get all the cheese
 *
 * @author Ken
 */
public class CheeseLister {

    private final CheeseDAO cheeseGetter;

    public CheeseLister() {
        cheeseGetter = new CheeseDAOImpl();
    }

    public ArrayList<CheeseBean> getSomeCheese() throws SQLException {
        return cheeseGetter.getAllCheeses();
    }
}
