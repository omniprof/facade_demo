package com.kenfogel.facade_demo.facade;

import com.kenfogel.facade_demo.data.CheeseAndTeaBean;
import com.kenfogel.facade_demo.listers.CheeseLister;
import com.kenfogel.facade_demo.listers.TeaLister;
import java.sql.SQLException;

/**
 * The Facade class that provides access to two independent classes. Facades are
 * coupled to what they represent so it must be clear that such a class is a
 * Facade
 *
 * @author Ken
 */
public class CheeseAndTeaFacade {

    private final CheeseLister cheese;
    private final TeaLister tea;

    public CheeseAndTeaFacade() {
        cheese = new CheeseLister();
        tea = new TeaLister();
    }

    public CheeseAndTeaBean cheeseAndTeaLister() throws SQLException {

        CheeseAndTeaBean catb = new CheeseAndTeaBean();
        catb.setCheese(cheese.getSomeCheese());
        catb.setTea(tea.getSomeTea());
        return catb;
    }
}
